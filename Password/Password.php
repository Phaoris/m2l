<?php
/**
* Génération d'un mot de passe au hazard
*/
class Password
{
    /**
     * Generation du MDP
     *
     * @access public
     * @param array $args
     * @return string
     **/
    public function generation($args = array())
    {
        $longueur     = (!array_key_exists('longueur', $args))     ? 15     :     $args['longueur'];
        $lowercase  = (!array_key_exists('lowercase', $args))  ? TRUE   :  $args['lowercase'];
        $capitalize  = (!array_key_exists('capitalize', $args))  ? TRUE   :  $args['capitalize'];
        $utiliserNbr = (!array_key_exists('utiliserNbr', $args)) ? TRUE   : $args['utiliserNbr'];
        $char_speciaux = (!array_key_exists('char_speciaux', $args)) ? '-_!?%$' : $args['char_speciaux'];
        
        $upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $lower = "abcdefghijklmnopqrstuvwxyz";
        $number = "0123456789";
        
        $longueur_cle = 0;
        $cle        = '';
        
        if($capitalize === TRUE){
            $longueur_cle += 26;
            $cle .= $upper;
        }
        if($lowercase === TRUE){
            $longueur_cle += 26;
            $cle .= $lower;
        }
        if($utiliserNbr === TRUE){
            $longueur_cle += 10;
            $cle .= $number;
        }
        if(!empty($char_speciaux)){
            $longueur_cle +=strlen($char_speciaux);
            $cle .= $char_speciaux;
        }
        for($i = 1; $i <= $longueur; $i++){
            $password .= $cle{rand(0,$longueur_cle-1)};
        }
        return $password;
    } /* Fin de la fonction generate */
} /* Fin de la Class Password */
