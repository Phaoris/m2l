<?php 
require 'Qrcode.php';
if (!empty($_POST['url'])) {
    $url = strip_tags($_POST['url']);

    if (filter_var($url, FILTER_VALIDATE_URL)) {
        $qr = new Qrcode;
        $filename = uniqid();
        $qr->save(150,'files/'.$filename);

        header('Content-type:image/png');
        echo file_get_contents('files/' . $filename . '.png');
    }
}

 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>QRcode</title>
  </head>
  <body>
    <div>
        <form action="" method="post" accept-charset="utf-8">
            <input type="text" name="url" id="url" placeholder='entrez une URL' />
            <input type="submit" name="submit" id="submit" value="Envoyer" />
        </form>
    </div>
  </body>
</html>
