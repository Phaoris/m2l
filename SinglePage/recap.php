<!DOCTYPE html>
    <html>
    <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Récapitulation de la réservation</title>
            <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="content">
            <div class="menu">
                <h1>Récapitulatif de la réservation:</h1>
                <hr />
                <div class="sousMenu">
                    <h3>Mot de passe wifi</h3>
                    <span>1234321</span>
                    <p>la salle sera disponible le jeudi 12 janvier à 08:00 jusqu'à 17:00</p>
                    <p>Nom de la salle</p>
                    <p>la personne qui a reservé: <strong>Mounir Bennacer</strong></p>
                    <h1>Récapitulatif de la réservation:</h1>
                    <hr />
                    <h3>Mot de passe wifi</h3>
                    <span>1234321</span>
                    <p>la salle sera disponible le jeudi 12 janvier à 08:00 jusqu'à 17:00</p>
                    <p>Nom de la salle</p>
                    <p>la personne qui a reservé: <strong>Mounir Bennacer</strong></p>
                </div>
                <div class="sousMenu">
                    <h3>Mot de passe de la porte</h3>
                    <span>12JHKJGBhgdeghb879uh</span>
                    <p>la salle sera disponible le jeudi 12 janvier à 08:00 jusqu'à 17:00</p>
                    <p>Nom de la salle</p>
                    <p>la personne qui a reservé: <strong>Mounir Bennacer</strong></p>
                    <h1>Récapitulatif de la réservation:</h1>
                    <hr />
                    <h3>Mot de passe wifi</h3>
                    <span>1234321</span>
                    <p>la salle sera disponible le jeudi 12 janvier à 08:00 jusqu'à 17:00</p>
                    <p>Nom de la salle</p>
                    <p>la personne qui a reservé: <strong>Mounir Bennacer</strong></p>
                </div>
                <div class="sousMenu">
                    <h3>Reservez votre salle:</h3>
                    <p>Vous pouvez vous aussi reserver votre salle</p>
                    <p>Pour cela, clickez ci dessous</p>
                    <button type="submit">CONTINUER</button>
                </div>
            </div>
        </div>
    </body>
</html>