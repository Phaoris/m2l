<?php
require_once "../defaultincludes.inc";

$db = new MysqliDb('localhost','root','root','mrbs');


 ?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Coming Soon Page</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../css/reset.css" />
    <link rel="stylesheet" href="../css/button-style.css" />
    <link rel="stylesheet" href="../css/styles.css" />

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- JS -->
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../js/countdown.js"></script>

    <script>

        $(document).ready(function(){
            $("#countdown").countdown({
                date: "02 Thusday 2014 17:30:54",
                format: "on"
            },

            function() {
                // callback function
            });
        });

    </script>
</head>
<body>

    <!-- LOGO -->
    <header class="container">
        <!-- <a href="" id="logo"><img src="../images/logo.png" alt="MRBS maison des ligues" /></a> -->
    </header>


    <!-- TIMER -->
    <div class="timer-area">

        <h1>MRBS</h1>
        <span id="reservation">
        <p>Voici le temps qu'il vous reste avant l'expiration des clés associé à la réservation.</p></span>
        <ul id="countdown">
            <li>
                <span class="days">00</span>
                <p class="timeRefDays">jours</p>
            </li>
            <li>
                <span class="hours">00</span>
                <p class="timeRefHours">heures</p>
            </li>
            <li>
                <span class="minutes">00</span>
                <p class="timeRefMinutes">minutes</p>
            </li>
            <li>
                <span class="seconds">00</span>
                <p class="timeRefSeconds">secondes</p>
            </li>
        </ul>

    </div> <!-- end timer-area -->



    <!-- SIGNUP -->
    <div class="container">

        <h2>Vous pouvez vous aussi réserver votre salle, pour cela clickez sur le bouton ci-dessous</h2>

        <div class="form-wrapper">
            <a href="../index.php" class="btn-3d red">Réserver</a>
        </div> <!-- end form-wrapper -->

    </div> <!-- end container -->

</body>
</html>
