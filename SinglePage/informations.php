<?php
session_name("MRBS_SESSID");
session_start();
ini_set('display_errors','1'); error_reporting(E_ALL);
/**
 * Created by PhpStorm.
 * User: xorcist
 * Date: 28/12/2013
 * Time: 13:25
 */

require_once "../defaultincludes.inc";
require_once "../mrbs_sql.inc";
require_once "../functions_view.inc";
require_once "../Database/MysqliDb.php";
$db = new MysqliDb('localhost','root','root','mrbs');
//echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';

$user = getUserName();

echo "<h1>" . $user . "</h1>";

$db->where('name',$user);
$data = $db->get('mrbs_users');

foreach ($data as $u) {
    $theID = $u['id'];
    $theEmail = $u['email'];
    $theName = $u['name'];
}

//echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';

// retrieve token
if (isset($_GET["token"]) && preg_match('/^[0-9A-F]{40}$/i', $_GET["token"])) {
    $token = $_GET["token"];
}
else {
    throw new Exception("Valid token not provided.");
    header( "refresh:5; url=/" ); //wait for 5 seconds before redirecting
}


// verify token
$db->where('token',$token);
$row = $db->get('mrbs_url');

if ($row) {
    foreach($row as $r){
        $deleteID = $r['user_id'];
        $deleteToken = $r['token'];
        $deleteTstamp = $r['tstamp'];
    }
    //extract($row);
    //var_dump($r);die();
}
else {
    throw new Exception("la page que vous tentez de joindre n'existe plus.");
}

// do one-time action here, like activating a user account
// ...

// delete token so it can't be used again

// $db->where('user_id', $deleteID);
// $db->where('token', $deleteToken);
// $db->where('tstamp',$deleteTstamp);
// if ($db->delete('mrbs_url')) echo 'successfully deleted';

// 1 jour mesuré en secondes = 60 seconds * 60 minutes * 24 hours
    $delta = 86400;
// Check to see if link has expired
if ($_SERVER["REQUEST_TIME"] - $deleteTstamp > $delta) {
    throw new Exception("Token has expired.");
}
// do one-time action here, like activating a user account
// ..
